#include<iostream>

class Demo{

        int x=10;
        int y=20;

        public:
        Demo(){

                std::cout<<"In constructor"<<std::endl;
        }
        void fun(){

                std::cout<<x<<std::endl;
                std::cout<<y<<std::endl;
        }
};

int main(){

        Demo obj;
	Demo *obj2=new Demo();

        obj.fun();  
	obj2->fun();

	delete obj2;

//	obj2->fun();

        return 0;
}

