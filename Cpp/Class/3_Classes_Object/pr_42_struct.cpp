#include<iostream>

class company{

	int countEmp=5000;
	std::string name="IBM";

	public :

	company(){

		std::cout<<"In company Constructor"<<std::endl;
	}
	void companyinfo(){

		std::cout<<countEmp<<std::endl;
		std::cout<<name<<std::endl;
	}
};

class Employee{

	int empId=10;
	float empSal=95.07f;

	public :

	Employee(){

		std::cout<<"In Employee Consructor"<<std::endl;
	}

	void empInfo(){

		company obj;

		std::cout<<empId<<std::endl;
		std::cout<<empSal<<std::endl;

		obj.companyinfo();

		//std::cout<<obj.countEmp<<std::endl;
		//std::cout<<obj.name<<std::endl;
	}
};

int main(){

	Employee *emp = new Employee();

	emp->empInfo();

	return 0;
}
