#include<iostream>

class Demo{

	public :

		int x=10;
		Demo(){

			std::cout<<"In Constructor"<<std::endl;
			std::cout<<x<<std::endl;
		}

		Demo(int x){

			this->x=x;
			std::cout<<"In Para Constructor"<<std::endl;
                        std::cout<<x<<std::endl;

			Demo();
		}
		void getData(){

                        std::cout<<x<<std::endl;
		}

		~Demo(){
			std::cout<<"Distructor"<<std::endl;
		}
};

int main(){

	Demo obj(50);

	std::cout<<"End Main"<<std::endl;
	return 0;
}
