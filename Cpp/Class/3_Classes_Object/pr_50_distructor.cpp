#include<iostream>

class Demo{

	public :

		Demo(){
                        std::cout<<"In Constructor"<<std::endl;
                }

		~Demo(){
                        std::cout<<"Distructor"<<std::endl;
                }
};

int main(){

	Demo obj1;

	Demo *obj2=new Demo();

	std::cout<<"End Main"<<std::endl;

	delete obj2;

	return 0;

}

