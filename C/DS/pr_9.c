#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Employee{

	int empId;
	char empName[20];
	float sal;
	struct Employee *next;
}Emp;

void main(){

	Emp *emp1=(Emp*)malloc(sizeof(Emp));
	Emp *emp2=(Emp*)malloc(sizeof(Emp));
	Emp *emp3=(Emp*)malloc(sizeof(Emp));

	emp1->empId=1;
	strcpy(emp1->empName,"Kahna");
	emp1->sal=60.0;
	emp1->next=emp2;

	emp2->empId=2;
	strcpy(emp2->empName,"Ashish");
	emp2->sal=80.0;
	emp2->next=emp3;

	emp3->empId=3;
	strcpy(emp3->empName,"Rahul");
	emp3->sal=50.0;
	emp3->next=NULL;

	printf("%d\n",emp1->empId);
	printf("%s\n",emp1->empName);
	printf("%f\n",emp1->sal);
	printf("%p\n",emp1->next);

	void acssesData(Emp *ptr){

        printf("%d\n",ptr->empId);
        printf("%s\n",ptr->empName);
        printf("%f\n",ptr->sal);
        printf("%p\n",ptr->next);

	}

	acssesData(emp2);
        acssesData(emp3);


}
