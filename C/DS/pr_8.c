#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct employee{

	int empid;
	char empname[20];
	float sal;
	struct employee *next;
}Emp;

void main(){


	Emp obj1,obj2,obj3;

	obj1.empid=1;
	strcpy(obj1.empname,"Kanha");
	obj1.sal=50.00;
	obj1.next=&obj2;

	obj2.empid=2;
        strcpy(obj2.empname,"Rhul");
        obj2.sal=60.00;
        obj2.next=&obj3;

	obj3.empid=3;
        strcpy(obj3.empname,"Ashish");
        obj3.sal=65.00;
        obj3.next=NULL;

	struct employee *head =&obj1;

	printf("%d\n",head->empid);
	printf("%s\n",head->empname);
	printf("%f\n",head->sal);

	printf("%d\n",obj1.next->empid);
        printf("%s\n",obj1.next->empname);
        printf("%f\n",obj1.next->sal);

	printf("%d\n",obj2.next->empid);
        printf("%s\n",obj2.next->empname);
        printf("%f\n",obj2.next->sal);
}



