#include<stdio.h>
#include<string.h>
#include<stdlib.h>

struct company{

	char cname[20];
	int Empcount;
	float revenue;
};

void main(){

	struct company *cptr=(struct company *)malloc (sizeof(struct company));

	strcpy(cptr->cname,"Veritas");
	cptr->Empcount=700;
	(*cptr).revenue=150.00;

	printf("%s\n",cptr->cname);
	printf("%d\n",cptr->Empcount);
	printf("%f\n",(*cptr).revenue);
}
