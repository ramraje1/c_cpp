#include<stdio.h>
#include<stdlib.h>

struct demo{

	int data;

	struct demo *next;
};

struct demo *head =NULL;

struct demo* createnode(){

	struct demo *newnode=(struct demo*)malloc(sizeof(struct demo));

	printf("Enter data : \n");
	scanf("%d",&newnode->data);

	newnode->next=NULL;

	return newnode;

}

void addnode(){

	struct demo *newnode=createnode();

	if(head==NULL){

		head =newnode;

	}else{
		struct demo *temp=head;

		while(temp->next!=NULL){

			temp=temp->next;
		}
		temp->next=newnode;
	}
}

void addfirst(){

struct demo *newnode=createnode();

if(head==NULL){

	head =newnode;
}else{
	newnode->next=head;
	head=newnode;
}
}

void printll(){

	struct demo *temp=head;

	while(temp!=NULL){

		printf("|%d|",temp->data);

		temp=temp->next;
	}

	printf("\n");
}

// void addAtpos(int pos){

// 	struct demo *newnode=createnode();
// 	struct demo  *temp=head;

// 	while(pos-2){

// 		temp=temp->next;
// 		pos--;
// 	}
// 	newnode->next=temp->next;
// 	temp->next=newnode;
// }

void deletefirst(){

	struct demo*temp=head;

	head=temp->next;
	free(temp);
}


void deletelast(){

	struct demo *temp=head;

	while(temp->next->next!=NULL){

		temp=temp->next;
	}

	free(temp->next);
	temp->next=NULL;
}

int countnode(){

	int count=0;

	struct demo *temp =head;

	while(temp!=NULL){

		count++;

		temp=temp->next;

	}
	return count;

}

int addAtpos(int pos){

	int count =countnode();

	if(pos<=0||pos>=count+2){

		printf("Invalid node position \n");
		return -1;

	}else{

		if(pos==count+1){

			addnode();
		}else{} 
		    if(pos==0){

			addfirst();

		}else{

			struct demo *newnode=createnode();
			struct demo *temp=head;

			while(pos-2){

				temp=temp->next;
				pos--;

			}
			newnode->next=temp->next;
			temp->next=newnode;
		}
		return 0;
	}
}

void main(){



	char choice;

	do{
		printf("1.addnode\n");
		printf("2.addfirst\n");
		printf("3.addAtpos\n");
		printf("4.printll\n");
		printf("5.deletefirst\n");
		printf("6.deletelast\n");
		

		int ch;

		printf("Enter Choice :\n");
		scanf("%d",&ch);

		switch (ch){

			case 1:
			       addnode();
			       break;

			case 2:
			       addfirst();
			       break;

			case 3:
			       { 
				int pos;
				printf("Enter position to add node : \n");
                scanf("%d",&pos);
				addAtpos(pos);
			       }
			       break;

			case 4:
			       printll();
			       break;

			case 5:

			       deletefirst();
			       break;

			case 6:
			      
			      deletelast();
			      break;

			default :
			         printf("Wrong choice \n");

		}

		getchar();
		printf("Do you want to continue y/n? \n");
		scanf("%c",&choice);

	}
	while(choice=='y'||choice=='Y');
}